iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

require asyn
require s7plc
require calc
require modbus

#s7plcConfigure(PLCname, IPaddr, port, inSize, outSize, bigEndian, recvTimeout, sendInterval)
s7plcConfigure("ESS_Skid", "172.16.60.38", "2000", 292, 122, 1, 5000, 1000)
var s7plcDebug 0

dbLoadRecords("$(E3_IOCSH_TOP)/db/cooling.db")
